---
regel:
  code: TR357
  documentatie: 'Als een wijziging niet in de toekomst moet ingaan, dan is het altijd
    een wijziging die voor de gehele looptijd geldt.

    '
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

