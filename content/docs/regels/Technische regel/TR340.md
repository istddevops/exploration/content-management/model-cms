---
regel:
  code: TR340
  documentatie: 'Indien de ProductCode in de Toewijzing gevuld is, dan moet in het
    declaratiebericht in de Prestatie de toegewezen ProductCode overgenomen worden.  '
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

