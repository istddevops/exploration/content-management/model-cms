---
regel:
  code: TR362
  documentatie: "Alleen als een toewijzing wordt gestuurd als honorering van een verzoek,\
    \ dan wordt ReferentieAanbieder overgenomen uit het verzoek. \nAls een toewijzing\
    \ wordt gestuurd  op initiatief van de gemeente wordt ReferentieAanbieder niet\
    \ gevuld.\nVoorbeeld: na een verzoek om toewijzing wordt een toewijzing gestuurd,\
    \ deze bevat dan ReferentieAanbieder. Na verloop van tijd stuurt de gemeenten\
    \ op haar initiatief een gewijzigde toewijzing, bijvoorbeeld met een Einddatum\
    \ ingevuld. Deze gewijzigde toewijzing heeft dan geen ReferentieAanbieder omdat\
    \ het geen honorering van een verzoek is.\n"
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

