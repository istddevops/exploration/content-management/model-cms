---
regel:
  code: OP191
  documentatie: "Commentaar mag in de berichten gebruikt worden om extra informatie\
    \ op te nemen. Het commentaar bevat een toelichting op de betreffende berichtklasse,\
    \ die niet elders in het bericht kan worden opgenomen. \nCommentaar in het bericht\
    \ mag geen tot een persoon herleidbare gegevens bevatten zonder toestemming van\
    \ die persoon. "
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

