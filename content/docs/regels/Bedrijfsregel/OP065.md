---
regel:
  code: OP065
  documentatie: "Clienten waarvan alle berichtklassen volledig zijn goedgekeurd worden\
    \ dus niet mee teruggestuurd in het retourbericht. Het retourbericht bevat alleen\
    \ clienten waarvan in \xE9\xE9n of meer berichtklassen, over of behorend bij die\
    \ client, fouten zijn geconstateerd of waarvan, in het geval van het declaratie-/factuurbericht,\
    \ het ingediende bedrag niet (volledig) wordt toegekend. In dat geval wordt de\
    \ berichtklasse Client inclusief alle onderliggende berichtklassen retour gezonden,\
    \ voorzien van retourcodes."
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

