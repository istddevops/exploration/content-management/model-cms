---
regel:
  code: OP087
  documentatie: "Deze regel is van toepassing indien voor 1 client  meerdere producten\
    \ toegewezen zijn. Alle geldige toewijzingen binnen 1 client, voor 1 aanbieder\
    \ worden in 1 toewijzingsbericht geplaatst en naar de betreffende aanbieder gestuurd\
    \ inclusief de toewijzingen die gewijzigd zijn sinds het laatst verstuurde toewijzingsbericht\
    \ voor deze client en aanbieder. Denk hierbij aan intrekkingen die zijn gedaan\
    \ en waar de aanbieder nog niet van op de hoogte is.   \n\nVoor iedere client\
    \ waarbij een toewijzing verandert, wordt een toewijzingsbericht gestuurd voor\
    \ alle actuele toewijzingen voor die aanbieder behorende bij die client ."
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

