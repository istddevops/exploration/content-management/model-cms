---
regel:
  code: OP348
  documentatie: 'Het toewijzingsbericht wordt gevuld conform IV093. De ingangsdatum
    en einddatum van de toewijzing kan afwijken van de in de VOW gevraagde data in
    het geval van toewijzen met terugwerkende kracht. Het ToewijzingNummer wijkt af
    indien voor de gevraagde wijziging een nieuwe toewijzing afgegeven moet worden
    (conform OP257 en IV066). '
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

