---
regel:
  code: RS032
  documentatie: "Bij datatypen van het type date is het niet toegestaan een Tijdszone\
    \ mee te geven. \nBijvoorbeeld: \n2016-08-30 is goed\n2016-08-30Z is niet goed"
  type: Constraint

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

