---
regel:
  code: IV056
  documentatie: "Een retourbericht op een declaratie- of factuurbericht wordt gestuurd\
    \ om de aanbieder te informeren over de beoordeling (technisch/inhoudelijk) van\
    \ het bericht. Daarnaast wordt de aanbieder geinformeerd over het totaalbedrag\
    \ dat door de gemeente wordt toegekend op de declaratie of factuur.  \n\nVoor\
    \ ieder verzonden declaratie- of factuurbericht wordt slechts \xE9\xE9n retourbericht\
    \ gestuurd (\xE9\xE9n-op-\xE9\xE9n). De gemeente stuurt altijd een retourbericht\
    \ naar de aanbieder.  \nBij afkeur op de technische controle (niveau 1 en 2, zie\
    \ IV032) wordt na aanpassing van de fouten een nieuw bericht ingediend. Het afgekeurde\
    \ bericht is niet verwerkt en ingediende prestaties dienen in dit geval dus niet\
    \ te worden gecrediteerd (in geval van factureren, bij declareren is crediteren\
    \ van afgewezen regels sowieso niet nodig).\nBij afkeur op de inhoudelijke controle\
    \ (niveau 3 en 4) worden de afgewezen prestaties, na aanpassing van de fouten\
    \ in een volgende declaratie of factuur verstuurd. \n\nDe vulling van het retourbericht\
    \ is afhankelijk van de situatie en de geconstateerde fouten: \n\n1.    Het bericht\
    \ is volledig goedgekeurd:\nAlleen de header wordt retour gestuurd. Retourcode\
    \ 8001 (Declaratie is volledig toegewezen) wordt meegegeven en het uiteindelijke\
    \ toegekende totaalbedrag wordt vermeld.  \n\n2.    Het bericht is volledig afgekeurd:\n\
    Alleen de header wordt retour gestuurd. Het toegekende totaalbedrag is nul. Het\
    \ bericht wordt als niet verzonden beschouwd en is niet verwerkt in de administratie\
    \ van de ontvanger. Daarnaast wordt een retourcode meegegeven afhankelijk van\
    \ de reden van afkeur:  \na.    Het bericht bevat een fout in de header of kan\
    \ niet worden afgeleverd bij de gemeente: retourcode 0001 (Bericht is afgekeurd\
    \ om technische reden).\nb.    Het factuurnummer is niet uniek: de retourcode\
    \ 0030 (Factuurnummer moet uniek zijn voor de verzendende partij) wordt teruggegeven.\n\
    c.    Er is een declaratie ingediend terwijl een factuur werd verwacht of andersom:\
    \ de retourcode 8855 resp. 8856 wordt teruggegeven.\nNB: Indien het volledige\
    \ factuurbericht is afgekeurd dient er geen creditnota te worden verstuurd. (zie\
    \ ook IV053).\n\n3.    Het bericht is gedeeltelijk afgekeurd:\nDe header wordt\
    \ retour gestuurd met retourcode 0200 (Geen opmerking over deze berichtklasse)\
    \ en het uiteindelijk toegekende totaalbedrag. Indien een fout is geconstateerd\
    \ in de Clientklasse, wordt de betreffende retourcode bij de Client gevuld en\
    \ worden alle onderliggende prestaties afgewezen met retourcode 0233 (Berichtklasse\
    \ is niet beoordeeld). Indien een ingediende Prestatie wordt afgewezen of (in\
    \ geval van declareren) deels wordt toegekend, wordt deze Prestatie retour gezonden.\
    \ Hierbij wordt tevens de bijbehorende klasse Client meegestuurd met retourcode\
    \ 0200 (Geen opmerking over deze berichtklasse). De vulling van de retour gezonden\
    \ Prestaties is afhankelijk van onderstaande situaties:\na.    Prestatie is afgewezen:\
    \ \xE9\xE9n of meerdere retourcodes worden gevuld om de inhoudelijke reden van\
    \ afwijzing aan te geven. GemeenteBerekendBedrag en ToegekendBedrag worden gevuld\
    \ met 0 (nul). \nb.    Bij een declaratie kan het voorkomen dat op de Prestatie\
    \ een afwijkend bedrag is toegekend: GemeenteBerekendBedrag en ToegekendBedrag\
    \ worden gevuld met het afwijkende bedrag dat de gemeente toekent op deze Prestatie.\
    \ Let op: dit is niet toegestaan bij factureren! De retourcode hoeft niet gevuld\
    \ te worden.\nNB: Prestaties waarvan het ingediende bedrag volledig wordt toegekend,\
    \ worden niet opgenomen in het retourbericht. \n\nHet retourbericht op een declaratie-\
    \ of factuurbericht dient niet: \nAls ontvangstbevestiging.  \nVoor het opvragen\
    \ van (extra) informatie door de gemeente. \nVoor het doorgeven van correcties\
    \ op factuurgegevens aan de aanbieder."
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

