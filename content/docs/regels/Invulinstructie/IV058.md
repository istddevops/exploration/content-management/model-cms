---
regel:
  code: IV058
  documentatie: "Bij meerdere identieke producten op \xE9\xE9n dag, waarbij alle overige\
    \ declaratie-/factuur-inhoudelijke gegevens identiek zijn, moet dit in het declaratie-\
    \ of factuurbericht worden samengevoegd tot \xE9\xE9n berichtklasse prestatie\
    \ (met als aantal het desbetreffende aantal). "
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

