module gitlab.com/istddevops/exploration/content-management/model-cms

go 1.19

require (
	gitlab.com/istddevops/shared/gohugo/gohugo-book-template v1.2.0 // indirect
	gitlab.com/istddevops/shared/gohugo/gohugo-book-theme v1.0.0 // indirect
	gitlab.com/istddevops/shared/gohugo/gohugo-kroki v1.1.0 // indirect
)
