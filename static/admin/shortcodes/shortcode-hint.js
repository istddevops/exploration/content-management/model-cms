CMS.registerEditorComponent({
    id: "hint-info",
    label: "Hint Info",
    fields: [{
            name: "body",
            label: "Body",
            widget: "string"
        },
    ],
    pattern: /^{{< hint info >}}([a-zA-Z0-9]+){{< \/hint >}}/,
    fromBlock: function(match) {
        return {
            body: match[1]
        };
    },
    toBlock: function(obj) {
        return `{{< hint info >}}\n${obj.body}\n{{< /hint >}}`;
    },
    toPreview: function(obj) {
        return '';
    },
});