# Doelstelling

De huidige **iStandaarden** beheren de **Informatiemodellen** met behulp van de huidige  `legacy tooling` (*BizzDesign*).

Deze `legacy tooling` is vanwege gebrekkige ondersteuning aan vervanging toe. Deze verkenning heeft als doel de mogelijkheden tot vervanging daarvan te onderzoeken. Dit op basis van beschikbare componenten uit het `open ecosysteem`.

# Aanpak

Uitgangspunten voor deze verkenning:

- Ontkoppel de `data` van `tools` door open data-formaten (YAML, JSON. TOML, XML) te gebruiken

- Gebruik `tools, raamwerken en/of componenten` die binnen een CI/CI-omgeving (GitLab, ...etc.) kunnen worden toegepast, gebruik maken van open data-formaten en worden ondersteund door een (groeiende) omvangrijke internationale community:

- De basis van de verkenning is een kopie van de **YAML-data** uit een eerdere uitgevoerde verkenning. Zie [iWmo FAIR Demo / Phyton](https://gitlab.com/istddevops/exploration/fair-publiceren/iwmo-fair-demo/-/tree/dev/python).

- Het [HUGO-raamwerk](https://gohugo.io) biedt *functionaliteit* om de **YAML-data** af te beelden als *PlantUML-diagrammen*. Zie als voorbeeld [iWmo FAIR Demo / Layouts ](https://gitlab.com/istddevops/exploration/fair-publiceren/iwmo-fair-demo/-/blob/dev/layouts).

- Het [Netlify CMS](https://www.netlifycms.org) biedt een **open raamwerk** om een maatwerk **contentbeheer-service** samen te stellen die op het [Netlify Platform](https://www.netlify.com/products) kan draaien.

> In de verkenning willen we een **demo** voor een *Contentbeheer-omgeving* voor **Informatiemodellen** samenstellen..


# To-Do

Taken en voortgang daarop.

- [ ] Layout van YAML-data naar Markdown-content
  - [ ] Berichten
    - [x] Partials Berichten
    - [ ] Partials Klassen
    - [ ] Partials Elementen
  - [ ] Datatypen
  - [ ] Codelijsten
  - [ ] Regels
  - [ ] Basisschema
- [ ] Bizz Conversie van YAML-data naar Markdown-content
  - [ ] Berichten
  - [ ] Datatypen
  - [ ] Codelijsten
  - [ ] Regels
  - [ ] Basisschema
- [ ] PlantUMLConfig functionaliteit toevoegen
